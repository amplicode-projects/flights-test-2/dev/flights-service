output "vpc_id" {
  value = data.yandex_vpc_network.default.id
}

output "subnet_id" {
  value = data.yandex_vpc_subnet.default.id
}
