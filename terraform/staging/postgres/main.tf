terraform {
  required_providers {
    yandex = {
      source  = "yandex-cloud/yandex"
      version = ">= 0.91.0"
    }
  }
  required_version = ">= 0.13"
}

locals {
  port = 6432
}

resource "random_password" "additional_db_passwords" {
  count   = var.random_password ? 1 : 0
  length  = 16
  special = false
}

resource "yandex_mdb_postgresql_cluster" "cluster-db-postgresql-main" {
  name                = "${var.name}-db-cluster"
  environment         = "PRODUCTION"
  network_id          = var.vpc_id
  deletion_protection = false
  security_group_ids  = [yandex_vpc_security_group.sg-db-postgresql-main.id]

  config {
    version = var.engine_version
    resources {
      disk_size          = var.storage
      disk_type_id       = "network-ssd"
      resource_preset_id = var.db_instance
    }
    performance_diagnostics {
      enabled                      = var.performance_diagnostics
      sessions_sampling_interval   = var.sessions_sampling_interval
      statements_sampling_interval = var.statements_sampling_interval
    }
  }

  host {
    zone             = var.zone
    subnet_id        = var.subnet_id
    assign_public_ip = false
  }

  labels = {
    name       = "${var.name}-db-cluster"
    managed-by = "terraform"
  }
}

resource "yandex_mdb_postgresql_database" "db-postgresql-main" {
  cluster_id = yandex_mdb_postgresql_cluster.cluster-db-postgresql-main.id
  name       = "${var.name}-db"
  owner      = var.username
  depends_on = [
    yandex_mdb_postgresql_user.user-postgresql-main
  ]
}

resource "yandex_mdb_postgresql_user" "user-postgresql-main" {
  cluster_id = yandex_mdb_postgresql_cluster.cluster-db-postgresql-main.id
  name       = var.username
  password   = var.random_password ? random_password.additional_db_passwords[0].result : var.password
}

resource "yandex_vpc_security_group" "sg-db-postgresql-main" {
  name       = "${var.name}-sg"
  network_id = var.vpc_id

  ingress {
    protocol       = "TCP"
    v4_cidr_blocks = ["0.0.0.0/0"]
    from_port      = local.port
    to_port        = local.port
  }

  egress {
    protocol       = "TCP"
    v4_cidr_blocks = ["0.0.0.0/0"]
    from_port      = local.port
    to_port        = local.port
  }

  labels = {
    name       = "${var.name}-sg"
    managed-by = "terraform"
  }
}
