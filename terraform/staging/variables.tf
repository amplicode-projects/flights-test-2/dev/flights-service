variable "name" {
    type    = string
    default = "staging"
}

variable "image" {
    type    = string
}

variable "port" {
    type    = number
    default = 8081
}

variable "instance_cores" {
    type    = number
    default = 2
}

variable "instance_memory" {
    type    = number
    default = 2
}

variable "platform" {
    type = string
    default = "standard-v1"
}

locals {
    env = [
        {
            name  = "LEGACYFLIGHTS2_DB_NAME"
            value = module.legacyflights2.name
        },
        {
            name  = "LEGACYFLIGHTS2_DB_HOST"
            value = module.legacyflights2.host
        },
        {
            name  = "LEGACYFLIGHTS2_DB_PORT"
            value = module.legacyflights2.port
        },
        {
            name  = "LEGACYFLIGHTS2_DB_USER"
            value = module.legacyflights2.username
        },
        {
            name  = "LEGACYFLIGHTS2_DB_PASSWORD"
            value = module.legacyflights2.password
        },
      {
        name  = "SPRING_PROFILES_ACTIVE",
        value = "staging"
      }
    ]
}

variable "folder_id" {
    type    = string
    default = ""
}

variable "zone" {
    type    = string
    default = "ru-central1-a"
}

variable "docker_logs" {
  type    = bool
  default = false
}

variable "extended_metrics" {
  type    = bool
  default = false
}

variable "alb_logs" {
  type    = bool
  default = false
}

variable "retention_period" {
  type    = number
  default = 1
}

variable "cidr_block" {
    type    = string
    default = "10.0.0.0/16"
}
