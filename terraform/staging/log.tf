resource "random_string" "random_string" {
    length  = 8
    special = false
    numeric = false
    upper   = false
}

resource "yandex_logging_group" "log" {
    count            = var.alb_logs || var.docker_logs ? 1 : 0
    name             = "logging-group-${random_string.random_string.result}"
    retention_period = "${var.retention_period}h"

    labels = {
        name       = "${var.name}-logging-group"
        managed-by = "terraform"
    }
}
