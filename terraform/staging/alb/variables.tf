variable "vpc_id" {
  type = string
}

variable "port" {
  type = number
}

variable "subnet_id" {
  type = string
}

variable "name" {
  type = string
}

variable "zone" {
  type = string
}

variable "alb_logs" {
  type = bool
}

variable "log_group_id" {
  type = string
}

variable "target_group_id" {
  type = string
}

variable "instance_sg_id" {
  type = string
}
