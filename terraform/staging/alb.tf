module "alb" {
    source = "./alb"

    alb_logs        = var.alb_logs
    instance_sg_id  = module.instance.instance_sg_id

    log_group_id    = 0

    name            = var.name
    subnet_id       = module.vpc.subnet_id
    target_group_id = module.instance.target_group_id
    vpc_id          = module.vpc.vpc_id
    zone            = var.zone
    port            = var.port
}
