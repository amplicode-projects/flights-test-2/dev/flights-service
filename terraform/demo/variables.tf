variable "name" {
  type        = string
  default     = "flightsdb22"
  description = "Application name"
}

variable "vpc_cidr_block" {
  type    = string
  default = "10.0.0.0/16"
}

variable "use_private_subnets" {
  type    = bool
  default = false
}

variable "azs_count" {
  type        = number
default     = 2

  description = "Number of Availability Zones to be used"
}

variable "instance_type" {
  type    = string
  default = "t2.medium"
}

variable "region" {
  type    = string
  default = "us-east-2"
}

variable "image" {
  type    = string
}

variable "ports" {
  type    = list(number)
  default = [ 8080 ]
}

locals {
  env = [
      {
        namespace = "aws:elasticbeanstalk:application:environment",
        name = "LEGACYFLIGHTS2_DB_NAME",
        value = module.legacyflights2_db.name
      },
      {
        namespace = "aws:elasticbeanstalk:application:environment",
        name = "LEGACYFLIGHTS2_DB_HOST",
        value = module.legacyflights2_db.host
      },
      {
        namespace = "aws:elasticbeanstalk:application:environment",
        name = "LEGACYFLIGHTS2_DB_PORT",
        value = module.legacyflights2_db.port
      },
      {
        namespace = "aws:elasticbeanstalk:application:environment",
        name = "LEGACYFLIGHTS2_DB_USER",
        value = module.legacyflights2_db.username
      },
      {
        namespace = "aws:elasticbeanstalk:application:environment",
        name = "LEGACYFLIGHTS2_DB_PASSWORD",
        value = module.legacyflights2_db.password
      },
      {
        namespace = "aws:elasticbeanstalk:application:environment",
        name = "SPRING_PROFILES_ACTIVE",
        value = "demo"
      }
  ]
}

variable "legacyflights2_db_name" {
  type    = string
  default = "legacyflights2"
}

variable "legacyflights2_db_engine" {
  type    = string
  default = "postgres"
}

variable "legacyflights2_db_engine_version" {
  type    = string
  default = "15.3"
}

variable "legacyflights2_db_instance_class" {
  type    = string
  default = "db.t3.small"
}

variable "legacyflights2_db_storage" {
  type    = number
  default = 10
}

variable "legacyflights2_db_user" {
  type    = string
  default = "root"
}

variable "legacyflights2_db_password" {
  type  = string
default = null

}

variable "legacyflights2_db_random_password" {
  type    = bool
  default = true
}

variable "legacyflights2_db_multi_az" {
  type    = bool
  default = false
}

