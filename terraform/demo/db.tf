module "legacyflights2_db" {
  source = "./db"

  name                                  = var.legacyflights2_db_name
  engine                                = var.legacyflights2_db_engine
  engine_version                        = var.legacyflights2_db_engine_version
  instance_class                        = var.legacyflights2_db_instance_class
  storage                               = var.legacyflights2_db_storage
  user                                  = var.legacyflights2_db_user
  password                              = var.legacyflights2_db_password
  random_password                       = var.legacyflights2_db_random_password
  vpc_id                                = module.vpc.id
  subnet_group_name                     = module.vpc.db_subnet_group_name
  multi_az                              = var.legacyflights2_db_multi_az
  source_security_group_id              = module.beanstalk.aws_security_group.id
}
