package com.demo.flightsdb2.controller

import com.demo.flightsdb2.domain.Airport
import com.demo.flightsdb2.domain.AirportRepository
import com.demo.flightsdb2.dto.AirportDto
import com.demo.flightsdb2.dto.AirportMapper
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RestController
import org.springframework.web.bind.annotation.RequestMapping

@RestController
@RequestMapping("/api/airport")
class AirportController(private val airportRepository: AirportRepository, private val airportMapper: AirportMapper) {
    @GetMapping
    fun getList(): List<AirportDto> {
        val airports: List<Airport> = airportRepository.findByOrderByNameAsc()
        val airportDtos: List<AirportDto> = airports.map(airportMapper::toDto)
        return airportDtos
    }

}

