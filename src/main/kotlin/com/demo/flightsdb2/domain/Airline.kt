package com.demo.flightsdb2.domain

import jakarta.persistence.*
import jakarta.validation.constraints.NotNull
import jakarta.validation.constraints.Size
import org.hibernate.annotations.JavaType
import org.hibernate.type.descriptor.java.BooleanJavaType

@Entity
@Table(name = "airdatas_airline", indexes = [
    Index(name = "airdatas_airline_code_key", columnList = "code", unique = true)
])
open class Airline : NamedEntity() {

    @Size(max = 31)
    @NotNull
    @Column(name = "code", nullable = false, length = 31)
    open var code: String? = null

    @Size(max = 255)
    @Column(name = "country")
    open var country: String? = null

    @JavaType(BooleanJavaType::class)
    @Column(name = "archived")
    open var archived: Boolean? = null
}